import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Homepage, Create, SingleNote, Archive } from "./pages";
import { Header } from "./commons";
import { NoteContextProvider } from "./context/notes";

function App() {
  return (
    <NoteContextProvider>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route path="/create" component={Create} />
          <Route path="/archive" component={Archive} />
          <Route path={`/notes/:id`} component={SingleNote} />
        </Switch>
      </Router>
    </NoteContextProvider>
  );
}

export default App;
