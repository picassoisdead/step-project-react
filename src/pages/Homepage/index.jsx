import React, { useContext } from "react";
import styled from "styled-components";
import { NoteContext } from "../../context/notes";
import { Container } from "../../commons";
import { Note } from "../../components";

const checkStatus = status => {
  if (status === "archive") {
    return true;
  } else {
    return false;
  }
};

export const Homepage = ({ history: { push } }) => {
  const { notes } = useContext(NoteContext);

  return (
    <Container>
      <h1>Homepage</h1>
      <NotesContainer>
        {notes.map(
          note =>
            !checkStatus(note.status) && (
              <Note
                onClick={() => {
                  push(`/notes/${note.id}`);
                }}
                key={note.id}
                note={note}
              />
            )
        )}
      </NotesContainer>
    </Container>
  );
};

const NotesContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
