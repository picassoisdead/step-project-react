import React, { useContext, useState } from "react";
import styled from "styled-components";
import { Modal, ModalButton } from "../../components";
import { NoteContext } from "../../context/notes";

export const SingleNote = ({
  history: { push },
  match: {
    params: { id },
  },
}) => {
  const [editing, setEditing] = useState(false);
  const { notes, deleteNote, editNote } = useContext(NoteContext);
  const [firstModalStatus, setFirstModalStatus] = useState(false);

  const toggleFirstModal = () => setFirstModalStatus((v) => !v);
  const note = notes.find((item) => item.id == id);

  const [fields, setFields] = useState({
    title: "",
    text: "",
    color: "#d32727",
  });

  const onChange = (e) => {
    const { name, value } = e.target;
    setFields((field) => ({
      ...field,
      [name]: value,
    }));
  };
  console.log(fields);
  const onSubmit = (e) => {
    e.preventDefault();
    editNote(id, { ...fields, status: note.status });
    setEditing(false);
  };

  return (
    <>
      {note && (
        <>
          <SingleNoteContainer>
            {!editing ? (
              <NoteContainer color={note.color}>
                <NoteTitle>{note.title}</NoteTitle>
                <NoteText>{note.text}</NoteText>
              </NoteContainer>
            ) : (
              <FormContainer>
                <form id="form1" onSubmit={onSubmit}>
                  <Input
                    type="text"
                    name="title"
                    value={fields.title}
                    onChange={onChange}
                  />
                  <Textarea
                    type="text"
                    name="text"
                    value={fields.text}
                    onChange={onChange}
                  />
                  <ColorContainer>
                    <h4>Color: </h4>
                    <RadioLabel color="#d32727">
                      <input
                        type="radio"
                        name="color"
                        value="#d32727"
                        checked={fields.color === "#d32727"}
                        onChange={onChange}
                      />
                      <span></span>
                    </RadioLabel>
                    <RadioLabel color="#3a2c84">
                      <input
                        type="radio"
                        name="color"
                        value="#3a2c84"
                        checked={fields.color === "#3a2c84"}
                        onChange={onChange}
                      />
                      <span></span>
                    </RadioLabel>
                    <RadioLabel color="#ef8e0b">
                      <input
                        type="radio"
                        name="color"
                        value="#ef8e0b"
                        checked={fields.color === "#ef8e0b"}
                        onChange={onChange}
                      />
                      <span></span>
                    </RadioLabel>
                    <RadioLabel color="#516f55">
                      <input
                        type="radio"
                        name="color"
                        value="#516f55"
                        checked={fields.color === "#516f55"}
                        onChange={onChange}
                      />
                      <span></span>
                    </RadioLabel>
                  </ColorContainer>
                </form>
              </FormContainer>
            )}

            <ButtonsContainer>
              {!editing ? (
                <Button
                  onClick={() => {
                    setEditing(true);
                    setFields((field) => ({
                      title: note.title,
                      text: note.text,
                      color: note.color,
                    }));
                  }}
                >
                  EDIT
                </Button>
              ) : (
                <button className="submit-button" type="submit" form="form1">
                  SAVE
                </button>
              )}
              {note.status == "archive" ? (
                <Button
                  onClick={() => {
                    editNote(id, {
                      status: "actual",
                      title: note.title,
                      text: note.text,
                      color: note.color,
                    });
                    push("/");
                  }}
                >
                  UNARCHIVE
                </Button>
              ) : (
                <Button
                  onClick={() => {
                    editNote(id, {
                      status: "archive",
                      title: note.title,
                      text: note.text,
                      color: note.color,
                    });
                    push("/");
                  }}
                >
                  ARCHIVE
                </Button>
              )}
              <Button
                onClick={() => {
                  toggleFirstModal();
                }}
              >
                DELETE
              </Button>
            </ButtonsContainer>
          </SingleNoteContainer>
          {firstModalStatus && (
            <div className="modal-background">
              <Modal
                header={"Confirmation"}
                closeButton={true}
                text={"Do you want to delete this note?"}
                close={toggleFirstModal}
                actions={[
                  <ModalButton
                    key={1}
                    backgroundColor="rgba(0,0,0, .2)"
                    text="Confirm"
                    onClick={() => {
                      toggleFirstModal();
                      deleteNote(note.id);
                      push("/");
                    }}
                  />,
                  <ModalButton
                    key={2}
                    backgroundColor="rgba(0,0,0, .2)"
                    text="Cancel"
                    onClick={toggleFirstModal}
                  />,
                ]}
              />
            </div>
          )}
        </>
      )}
    </>
  );
};

const SingleNoteContainer = styled.div`
  display: flex;
  max-width: 800px;
  padding: 40px 15px;
  margin: 0 auto;
`;
const NoteContainer = styled.div`
  width: calc(100% * 3 / 4);
  border: 1px solid white;
  min-height: 190px;
  border-radius: 7px;
  background: ${(p) => p.color};
  color: white;
`;

const NoteTitle = styled.h2`
  border-bottom: 1px solid white;
  margin: 0;
  padding: 20px;
  text-align: center;
`;
const NoteText = styled.p`
  margin: 0;
  padding: 20px;
  word-wrap: break-word;
`;

const ButtonsContainer = styled.div`
  width: calc(100% * 1 / 4);
  display: flex;
  flex-direction: column;
  margin-left: 20px;
`;
const Button = styled.button`
  background-color: white;
  color: black;
  border-radius: 10px;
  border: 1px solid black;
  padding: 20px;
  margin: 0 0 10px;
  cursor: pointer;
`;

const FormContainer = styled.div`
  width: calc(100% * 3 / 4);

  padding: 30px 20px;
  background-color: #e4e9ef;
  border-radius: 15px;
`;

const inputStyles = `
  display: block;
  border: 2px solid transparent;
  transition: all 0.3s ease;
  width: 100%;
  padding: 10px 10px;
  border-radius: 5px;
  background-color: white;
  &:focus {
    border-color: #d32727;
    outline: none;
  }`;
const Input = styled.input`
  ${inputStyles}
`;
const Textarea = styled.textarea`
  ${inputStyles};
  height: 100px;
  margin-top: 10px;
  resize: none;
`;

const ColorContainer = styled.div`
  display: flex;
  align-items: center;
  margin: 10px 0;
  h4 {
    margin: 0 25px 0 0;
  }
`;
const RadioLabel = styled.label`
  input {
    display: none;
  }
  span {
    display: inline-block;
    width: 30px;
    height: 30px;
    border-radius: 100%;
    margin: 0 10px;
    background-color: ${(p) => p.color};
    border: 4px solid transparent;
    transition: all 0.3s ease;
    cursor: pointer;
  }

  input:checked + span {
    border-color: white;
  }
`;
