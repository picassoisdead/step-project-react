import React, { createContext, useReducer, useEffect } from "react";

export const NoteContext = createContext();

const ADD_NOTE = "ADD_NOTE";
const GET_NOTE = "GET_NOTE";
const DELETE_NOTE = "DELETE_NOTE";
const EDIT_NOTE = "EDIT_NOTE";

const initialState = {
  notes: [],
};

const changeArray = (array, { data, id }) => {
  let arr = array;
  console.log(arr);

  for (let i = 0; i < arr.length; i++) {
    if (id == array[i].id) {
      arr[i].title = data.title;
      arr[i].text = data.text;
      arr[i].color = data.color;
      arr[i].status = data.status;
    }
  }
  return arr;
};

function reducer(state, { type, payload }) {
  switch (type) {
    case EDIT_NOTE:
      return {
        ...state,
        notes: changeArray(state.notes, { ...payload }),
      };
    case DELETE_NOTE:
      return {
        ...state,
        notes: state.notes.filter((p) => p.id !== payload),
      };
    case GET_NOTE:
      return {
        ...state,
        notes: payload,
      };
    case ADD_NOTE:
      return {
        ...state,
        notes: [...state.notes, payload],
      };
    default:
      return state;
  }
}

export const NoteContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const getNotes = async () => {
    const res = await fetch("http://localhost:3001/notes");
    const data = await res.json();
    dispatch({
      type: GET_NOTE,
      payload: data,
    });
  };

  const deleteNote = async (id) => {
    const options = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
    };
    fetch(`http://localhost:3001/notes/${id}`, options);
    dispatch({
      type: DELETE_NOTE,
      payload: id,
    });
  };

  const addNote = async (data) => {
    const options = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(data),
    };
    fetch("http://localhost:3001/notes", options);
    dispatch({
      type: ADD_NOTE,
      payload: data,
    });
  };

  const editNote = async (id, data) => {
    fetch(`http://localhost:3001/notes/${id}`, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "PATCH",
      body: JSON.stringify(data),
    });
    dispatch({
      type: EDIT_NOTE,
      payload: { data, id },
    });
  };

  useEffect(() => {
    getNotes();
  }, []);

  return (
    <NoteContext.Provider value={{ ...state, addNote, deleteNote, editNote }}>
      {children}
    </NoteContext.Provider>
  );
};
